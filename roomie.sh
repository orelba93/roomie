#!/bin/bash

APP="Roomie"

CLEAN="clean"
RUN="start"
INIT="init"
INIT_SERVER="init_server"
STOP="stop"
IP="IP"

if [ "$#" -eq 0 ] || [ $1 = "-h" ] || [ $1 = "--help" ]; then
    echo "Usage: ./runner [OPTIONS] COMMAND [arg...]"
    echo "       ./runner [ -h | --help ]"
    echo ""
    echo "Options:"
    echo "  -h, --help    Prints usage."
    echo ""
    echo "Commands:"
    echo "  $CLEAN       - Stop and Remove $APP containers."
    echo "  $INIT        - Build and Run $APP."
    echo "  $INIT_SERVER - Build and Run $APP web server."
    echo "  $RUN         - Run $APP."
    echo "  $STOP        - Stop $APP."
    echo "  $IP          - Get $APP IP. "
    exit
fi

clean() {
  stop_existing
  remove_stopped_containers
  remove_unused_volumes
}

get_ip() {
  echo "IP: "
  docker inspect -f "{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}} " $CURRENT_APP

}

init() {
  echo "Cleaning..."
  clean
  
  echo "Running docker..."
  docker-compose up --build
}

init_server(){
  SERVER="$(docker ps --all --quiet --filter=name=server)" 
  if [ -n "$SERVER" ]; then
    docker stop $SERVER
  fi
  remove_stopped_containers
  docker-compose up --build server


}

run(){
  docker-compose up
  docker inspect -f '{{range .NetworkSettings.Networks}}.{{.IPAddress}}{{end}}' server
}

stop_existing() {
  SERVER="$(docker ps --all --quiet --filter=name=server)"
  REDIS="$(docker ps --all --quiet --filter=name=redis)"
  ENTITIES="$(docker ps --all --quiet --filter=name=entities)"
  POSTGRES="$(docker ps --all --quiet --filter=name=postgres)"

  if [ -n "$SERVER" ]; then
    docker stop $SERVER
  fi

  if [ -n "$REDIS" ]; then
    docker stop $REDIS
  fi

  if [ -n "$ENTITIES" ]; then
    docker stop $ENTITIES
  fi
  
  if [ -n "$POSTGRES" ]; then
    docker stop $POSTGRES
  fi
}

remove_stopped_containers() {
  CONTAINERS="$(docker ps -a -f status=exited -q)"
	if [ ${#CONTAINERS} -gt 0 ]; then
		echo "Removing all stopped containers."
		docker rm $CONTAINERS
	else
		echo "There are no stopped containers to be removed."
	fi
}

remove_unused_volumes() {
  CONTAINERS="$(docker volume ls -qf dangling=true)"
	if [ ${#CONTAINERS} -gt 0 ]; then
		echo "Removing all unused volumes."
		docker volume rm $CONTAINERS
	else
		echo "There are no unused volumes to be removed."
	fi
}

if [ $1 = $CLEAN ]; then
  echo "Cleaning..."
	clean
	exit
fi

if [ $1 = $RUN ]; then
	run
	exit
fi

if [ $1 = $INIT ]; then
	init
	exit
fi

if [ $1 = $INIT_SERVER ]; then
	init_server
	exit
fi

if [ $1 = $STOP ]; then
	stop_existing
	exit
fi

if [ $1 = $IP ]; then
	CURRENT_APP=$2
	get_ip
	exit
fi

