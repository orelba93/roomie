import sys
import threading
import yaml

from src.modules.climate.updator import Updator
from src.modules.ir_controller.control import Control
# from src.utils.bte_server import BTEServer


# t = threading.Thread(target=BTEServer,args=(processor_type,))

with open('roomie.yaml') as f:
    room_conf = yaml.load(f,Loader=yaml.FullLoader)
    room_id = room_conf['id']
    namespace = room_conf['namespace']
    kafka = room_conf['kafka']
    if 'climate' in room_conf:
        sensor = room_conf['climate']['sensor']
        pin = room_conf['climate']['pin']
        
threads = []
try:
    threads.append(
        Updator(pin, sensor, room_id, namespace, kafka['server'])
    )
    threads.append(
        Control(kafka['server'], room_id)
    )
except RuntimeError as e:
    print("err 1", e)

for t in threads:
    try:
        t.start()
    except RuntimeError as err:
        print("err", err)
        continue
# print(bte_available,processor_type)
# t.start()
