import sys, time, bluetooth, subprocess
from uuid import getnode
import gatt

class AnyDeviceManager(gatt.DeviceManager):
    def device_discovered(self, device):
        print("Discovered [%s] %s" % (device.mac_address, device.alias()))


uuid = getnode()

class BTEServer():
    def __init__(self, name = "BTE Server", search_for="OnePlus 5T"):
        self.name           = name
        # self.address       = '58:00:E3:53:5A:F8'
        # self.server_sock    = bluetooth.BluetoothSocket( bluetooth.RFCOMM )
        # port = 13         # RFCOMM port
        # nearby_devices = bluetooth.discover_devices()

        manager = AnyDeviceManager(adapter_name='hci0')
        manager.start_discovery()
        manager.run()

        # for bdaddr in nearby_devices:
        #     if search_for == bluetooth.lookup_name( bdaddr ):
        #         target_address = bdaddr
        #         break
        
        # try:
        #     self.server_sock = bluetooth.BluetoothSocket( bluetooth.RFCOMM )
        #     self.server_sock.bind(("", bluetooth.PORT_ANY))
        #     self.server_sock.listen(1)

        #     port = self.server_sock.getsockname()[1]
        #     uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"

        #     bluetooth.advertise_service(self.server_sock, "SampleServer",
        #                     service_id = uuid,
        #                     service_classes = [ uuid, bluetooth.SERIAL_PORT_CLASS ],
        #                     profiles = [ bluetooth.SERIAL_PORT_PROFILE ], 
        #                     )
        # except Exception as err: 
        #     print("Bind failed Err", err)

    def register_service(
        self,
        service_id = uuid,
        service_classes = [ bluetooth.LAN_ACCESS_CLASS, bluetooth.SERIAL_PORT_CLASS ],
        profiles = [ bluetooth.SERIAL_PORT_PROFILE, bluetooth.LAN_ACCESS_PROFILE ]
#       protocols = [ OBEX_UUID ] 
    ):
        print("Waiting for connection on RFCOMM channel %d" % self.port)
        try:
            client_sock, client_info = self.server_sock.accept()
            print("Accepted connection from ", client_info, self.name)
            bluetooth.advertise_service(self.server_sock, self.name , service_id=service_id, service_classes=service_classes, profiles=profiles)
            print("advertised")
            while True:
                data = client_sock.recv(1024)
                if len(data) == 0: continue
                print("received [%s]" % data)
                self.handle_message(data)
        except IOError as e:
            print('err', e)

        print("disconnected")

        client_sock.close()
        self.server_sock.close()
        print("all done")


    def handle_message(self,msg):
        print("%s Going to handle message %s", self.name, msg)
        # self.handler(msg)
