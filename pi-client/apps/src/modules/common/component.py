import threading
import yaml
import json
import requests

class Component(threading.Thread):
    def __init__(self,component_type,  room_id):
        super().__init__()
        self.type = component_type
        self.room_id = room_id
        self.register()

    def register(self):
        with open('./roomie.yaml') as f:
            conf = yaml.load(f, Loader= yaml.FullLoader)
            server_conf = conf['server']
        endpoint = server_conf['url'] + self.room_id
        data = {"type": self.type, "id": "_".join([self.room_id, self.type])}
        res = requests.patch(endpoint, data= json.dumps(data), headers={"Content-Type":"application/json"})
        print(res.json())