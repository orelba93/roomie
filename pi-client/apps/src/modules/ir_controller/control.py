import subprocess
import threading
from ..common import component

from uuid import getnode as get_mac
from kafka import KafkaConsumer
# import socketio

# from webthing import Thing, Property, Value, WebThingServer, SingleThing
CONTROL_ACTION = 'IOT_IR'
NAMESPACE = 'roomie'
thread = None
server = None
# sio = socketio.Client( reconnection_attempts = 10)


class Control(component.Component):

    def __init__(self, SERVER_URL, room_id):
        super().__init__('IOT_control', room_id)
        self.consumer = KafkaConsumer(
            '_'.join([CONTROL_ACTION, self.room_id]),
            group_id=NAMESPACE,
            bootstrap_servers=SERVER_URL
        )

    def run(self):
        for msg in self.consumer:
            print(msg)
            self.run_command(msg)

        # sio = socketio.Client()
        # sio.connect(SERVER_URL,namespaces = [NAMESPACE])
        # sio.emit('JOIN_ROOM', {"roomId": self.id})

    def run_command(self, command):
        process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()
        print(output)
        print(error)

# def init():
#     AC = Thing(
#         'urn:dev:ops:ac-1',
#         'Air conditioner',
#         ['RemoteControl', 'Air Conditioner'],
#         'A web connected Air conditioner'
#     )

#     AC.add_property(
#         Property(
#             AC,
#             'on',
#             Value(True, lambda v: print 'On-State is now', v ),
#             metadata={
#                 '@type': 'OnOffProperty',
#                 'title': 'On/Off',
#                 'type': 'boolean',
#                 'description': 'Whether the air conditioner is turned on',
#             }
#         )
#     )


#     AC.add_property(
#         Property(
#             AC,
#             'swing',
#             Value(True, lambda v: print 'Swing-State is now', v ),
#             metadata={
#                 '@type': 'OnOffProperty',
#                 'title': 'On/Off',
#                 'type': 'boolean',
#                 'description': 'Whether the air conditioner swing mode is turned on',
#             }
#         )
#     )

#     AC.add_property(
#         Property(
#             AC,
#             'temperature',
#             Value(True, lambda v: print 'Temperature now', v ),
#             metadata={
#                 '@type': 'LevelProperty',
#                 'title': 'temperature',
#                 'type': 'integer',
#                 'description': 'air conditioner temperature',
#             }
#         )
#     )

#     server = WebThingServer(SingleThing(AC), port=8888)
#     server.start()

# try:
#     thread = threading.Thread(target = init, daemon=True)
# except KeyboardInterrupt:
#     thread.stop()
#     server.stop()
