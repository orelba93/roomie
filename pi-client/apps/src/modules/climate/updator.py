#!/usr/bin/python

from kafka import KafkaProducer
from uuid import getnode as get_mac
import time
import sys
import json

from ..common import component
from .Adafruit_DHT import common as DHT


sensor_args = {'11': DHT.DHT11,
               '22': DHT.DHT22,
               '2302': DHT.AM2302}


class Updator(component.Component):
    SERVER_URL = 'http://localhost/'
    UPDATOR_ACTION = 'IOT_CLIMATE'

    def __init__(self, pin, sensor, room_id, namespace, server):                
        super().__init__(Updator.UPDATOR_ACTION, room_id)
        self.pin = pin
        self.sensor = sensor_args[str(sensor)]
        self.humidity = None
        self.temperature = None
        self.__producer = KafkaProducer(
            bootstrap_servers=server,
            compression_type='gzip',
            value_serializer=lambda v: json.dumps(v).encode('utf-8'),
        )

    #socket updator
    def updateCurrentStatus(self, temperature, humidity):
        '''This update and publish the current temp and humidity'''
        print('Going to emit ' + Updator.UPDATOR_ACTION)
        self.__producer.send(Updator.UPDATOR_ACTION,
                             value={
                                 "temperature":  temperature,
                                 "humidity":     humidity,
                             },
                             headers=[("room_id", bytes(self.room_id, "utf8"))]
                             )

    def check(self):
        # Try to grab a sensor reading.  Use the read_retry method which will retry up
        # to 15 times to get a sensor reading (waiting 2 seconds between each retry).
        humidity, temperature = DHT.read_retry(self.sensor, self.pin)
        if self.humidity == humidity and self.temperature == temperature:
            return

        self.humidity = humidity
        self.temperature = temperature
    # Un-comment the line below to convert the temperature to Fahrenheit.
    # temperature = temperature * 9/5.0 + 32

    # Note that sometimes you won't get a reading and
    # the results will be null (because Linux can't
    # guarantee the timing of calls to read the sensor).
    # If this happens try again!
        if humidity is not None and temperature is not None:
            jsonObj = {"temp": '{0:0.1f}'.format(
                temperature), "humidity": '{0:0.1f}'.format(humidity)}
            print('Temp={0:0.1f}\nHumidity={1:0.1f}%'.format(
                temperature, humidity))
            self.updateCurrentStatus(jsonObj["temp"], jsonObj["humidity"])
        else:
            print('Failed to get reading. Try again!')

    def run(self):
        self.updateCurrentStatus(17,.56)
        while True:
            self.check()
            time.sleep(15)
