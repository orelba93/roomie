export class ClimateStatus{
    constructor(
        public label: string,
        public temperature: number,
        public state?: string,
        public humidity?: number,
        public roomId?: string,
        public timestamp?: number,
        public country?: string,
        public city?: string
    ){}

}