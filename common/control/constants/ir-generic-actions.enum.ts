export enum IR_GENERIC_ACTIONS {
    POWER_ON = 'POWER_ON',
    POWER_OFF = 'POWER_OFF',
    IR_EVENT = 'IR_EVENT'
}