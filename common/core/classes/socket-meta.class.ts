export class SocketMeta{
    constructor(
        public timestamp: number = new Date().getTime()
    ){}
}