import { SocketMeta } from "./socket-meta.class";

export class SocketMessage <T = any>{
    constructor(
        public type: string, 
        public data: T,
        public meta: SocketMeta = new SocketMeta){}
}