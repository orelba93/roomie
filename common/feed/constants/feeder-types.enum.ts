export enum FeederTypes{
    CLIMATE = "CLIMATE",
    IOT_CLIMATE = "IOT_CLIMATE",
    IOT_CONTROL = "IOT_CONTROL",
    NEWS = "NEWS",
    REMINDERS = "REMINDERS",
    LIST = "LIST"
}