/** 
 * ## Fibonacci 
 * fibonnaci returns
 * Fn = n-1 + n-2....3+2+1+1
 * 
**/

/**
 * recursive 
 * 
 * 4 = 3
 * @param {int} numberToCalc 
 */
function fibonacci(numberToCalc: number){
    if(numberToCalc <= 2){
        return 1;
    }
    return fibonacci(numberToCalc - 1) + fibonacci(numberToCalc - 2);
}

/**
 * iterative
 * @param {int} numberToCalc 
 */
function fibonacciIter(numberToCalc: number){
    let newSum: number = 1, 
        doublePrevSum: number = 1, 
        prevSum: number = 1;
    for( let i: number = 2; i < numberToCalc; i++ ){
        newSum = doublePrevSum + prevSum;
        doublePrevSum = prevSum;
        prevSum = newSum;
    }
    return newSum;

}

// eitan.revach@cloudinary.com 

console.assert(fibonacci(2) == 1,prepareData([2,1]))
console.assert(fibonacci(5) == 5,prepareData([5,5]))
console.assert(fibonacci(6) == 8,prepareData([6,8]))
console.assert(fibonacciIter(2) == 1,prepareData([2,1]))
console.assert(fibonacciIter(3) == 2,prepareData([3,2]))
console.assert(fibonacciIter(5) == 5,prepareData([5,5]))
console.assert(fibonacciIter(6) == 8,prepareData([6,8]))

function prepareData(data: any[]): string{
    return data.join();
}