import { ServerModule, getConfiguration } from "@sugoi/server"
import { RedisProvider } from "@sugoi/redis/dist";
import { FeedModule } from "../modules/feed/feed.module";
import { ControlModule } from "../modules/control/control.module";
import { ClimateModule } from "..//modules/climate/climate.module";
import { CoreModule } from "../modules/core/core.module";
import { AuthorizationModule } from "../modules/authorization/authorization.module";
import { UserModule } from "../modules/user/user.module";
import { ValuesetModule } from "../modules/valueset/valueset.module";


const REDIS_CONFIG = getConfiguration("DB").REDIS

@ServerModule({
    modules:[
        CoreModule,
        AuthorizationModule,
        ValuesetModule,
        FeedModule,
        ControlModule,
        ClimateModule,
        UserModule
    ],
    controllers:[],
    services:[
        // {
        //      provide:  RedisProvider.CreateConnection({
        //         url: process.env.REDIS_URL,
        //         isDefault: true
        //     }),
        //     useName: "RedisHandler"
        // },
    ]

})
export class BootstrapModule{}
