import { defaultErrorHandler, HttpServer, getConfiguration } from "@sugoi/server";
import * as bodyParser from 'body-parser';
import * as compression from 'compression';
import * as path from "path";
import { paths } from "../config/paths";
import { BootstrapModule } from "./bootstrap.module";
import { PassportAuthorization } from "../modules/authorization/strategies/passport-base.strategy"
import { MongoModel } from "@sugoi/mongodb";
import * as cookieParser from 'cookie-parser';



const DEVELOPMENT = process.env.NODE_ENV.includes('dev');
const TESTING = process.env.NODE_ENV.includes('test');

const setDBs = function (app) {
    MongoModel.setConnection(getConfiguration('DB').MONGODB as any).catch(console.error);
    console.debug(getConfiguration('DB'));

};


const server: HttpServer = HttpServer.init(BootstrapModule, "/", PassportAuthorization.of())
    .setStatic(paths.staticDir) // set static file directory path
    .setMiddlewares((app) => {
        app.disable('x-powered-by');
        app.set('etag', 'strong');
        app.set('host', process.env.HOST || '0.0.0.0');
        app.use(cookieParser())
        app.use(bodyParser.json());
        app.use(compression());
        app.use((req,res,next) => {
            console.log("[Incoming]",req.url,req.method);
            if(DEVELOPMENT){
                res.header('Access-Control-Allow-Origin', '*');
                res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
            }
            next()
        })

        setDBs(app);
    })
    .setErrorHandlers((app) => {
        app.use((req, res, next) => {
            // Set fallback to send the web app index file
            return res.sendFile(path.resolve(paths.index))
        });
        // The value which will returns to the client in case of an exception
        app.use(console.error);
        app.use(defaultErrorHandler(DEVELOPMENT || TESTING));
    });

export {server};
