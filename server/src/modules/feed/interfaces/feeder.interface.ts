import { FeedItem } from "../classes/feed-item.class";
import {FeederTypes} from "@roomie/common/feed";

export interface IFeeder<T extends FeedItem>{
    setType(type: FeederTypes): IFeeder<T>;
    type: FeederTypes
    query(payload?: any): Promise<{data: T, type: FeederTypes, corrolationId: string}>;
}