import {
    Controller,
    Request,
    RequestParam,
    HttpPost,
    RequestBodySchemaPolicy,
    ComparableSchema,
    SchemaTypes,
    RequestBody,
    AuthProvider,
    Authorized,
    Cookies,
    HttpPut,
    HttpPatch,
    RequestParamsSchemaPolicy
} from "@sugoi/server";
import { FeedService } from "../services/feed.service";
import { FeedProvider } from "../services/feed.provider";
import { FeederTypes } from "../../../../../common/feed";
import { RoomModel } from "../../authorization/models/room.model";
const NON_EMPTY_VALIDATOR = ComparableSchema
    .ofType(SchemaTypes.STRING)
    .setArrayAllowed(false)
    .setMandatory(true)
    .setRegex("(?!^$)");
@Controller('/feed')
// @Authorized()
export class FeedController {

    constructor(
        private feedService: FeedService,
        private feedProvider: FeedProvider
    ) {
    }


    @HttpPost("/status/:roomId?")
    // @RequestBodySchemaPolicy({
    //     features:           ComparableSchema.ofType({
    //                 type:       NON_EMPTY_VALIDATOR
    //  })
    //             .setForceArray(true)
    //             .setMandatory(true)
    // })
    async status(
        @Request() req: Request,
        @Cookies('roomie-at') userId: string,
        @RequestParam("roomId") roomId: string,
        @RequestBody() payload: { features: Array<{ type: FeederTypes, payload: any }> }
    ) {
        // const userData: User = await authProvider.getUser(req as any,null, null);


        // let feedsRequests = [];
        // if(payload && Array.isArray(payload.features)){
        //     const feed$ = payload.features.map(feature => {
        //         const feeder = this.feedProvider.getFeeder(feature.type);
        //         return feeder ? feeder.query(feature.payload) : Promise.resolve(null);
        //     });
        //     const room = new RoomModel("Global","GLOBAL", payload.features);
        //     feedsRequests.push(Promise.all(feed$).then(feeds => {
        //         room['feed'] = feeds.filter(feed => !!feed);
        //         return room;
        // }));
        // }

        return await this.feedService.getFeed(userId, roomId);

    }

    @HttpPut('/feature/:roomId/:featureId?')
    async updateFeature(
        @RequestBody() feature: any,
        @RequestParam('roomId') roomId: string,
        @RequestParam('featureId') featureId: string
    ) {
        const room: RoomModel = await RoomModel.findById(roomId);
        room.updateFeature(featureId, feature)
        try {
            return await room.update();
        } catch (e) {
            console.error(e)
            return 500;
        }
    }

    @HttpPatch('/:roomId')
    @RequestParamsSchemaPolicy({'roomId': NON_EMPTY_VALIDATOR})
    @RequestBodySchemaPolicy({
        'type': NON_EMPTY_VALIDATOR,
        'id': NON_EMPTY_VALIDATOR
    })
    async patchFeed(
        @RequestParam('roomId') roomId: string,
        @RequestBody() feed: { type: FeederTypes, id: string },
    ) {
        console.log('feed', feed)
        const provider = this.feedProvider.getFeeder(feed.type)
        if (!provider) {
            console.log("no provider", feed);
            return 422
        }

        let room: RoomModel;
        try {
            room = await RoomModel.findById(roomId);
        } catch (e) {
            console.error(e);
            return 500;
        }
        room.updateFeature(feed.id, feed);
        return await room.update();

    }

}