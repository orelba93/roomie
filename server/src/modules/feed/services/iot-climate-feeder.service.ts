import { Injectable } from "@sugoi/server";
import { IFeeder } from "../interfaces/feeder.interface";
import { FeederTypes } from "../../../../../common/feed/constants/feeder-types.enum";
import { ClimateStatus } from "../../../../../common/climate/classes/climate.model";
import { ClimateStatusModel } from "../../climate/model/climate-status.model";
import { QueryOptions, SortItem, SortOptions } from "@sugoi/mongodb";

@Injectable()
export class IOTClimateFeederService implements IFeeder<ClimateStatus>{

    readonly type: FeederTypes = FeederTypes.CLIMATE;
    
    constructor(){
    }

    setType(type: FeederTypes): IFeeder<any> {
        return this;
    }
    
    async query(payload?: {roomId: string}): Promise<{ data: ClimateStatus; type: FeederTypes; corrolationId: string; }> {
        const data = await ClimateStatusModel.findByContext({
            ids: [payload.roomId],
            options: new QueryOptions().setSortOptions(new SortItem(SortOptions.DESC, 'meta.created'))
        }).then( res => res && res.length > 0? res[0]: null) as ClimateStatus;
        return {data, type: this.type, corrolationId: ''};
    }
    


}