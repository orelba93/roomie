import { Injectable } from "@sugoi/server";
import { IFeeder } from "../interfaces/feeder.interface";
import { FeederTypes } from "@roomie/common/feed";

@Injectable()
export class DefaultFeeder<T = any> implements IFeeder<T>{
    public type: FeederTypes

    constructor(){

    }

    setType(type: FeederTypes): IFeeder<T> {
        this.type = type; 
        return this;
    }

    public async query(payload?: any): Promise<{data: T, type: FeederTypes, corrolationId: string}>{
        return {data: payload, type: this.type, corrolationId: ''}
    };
}