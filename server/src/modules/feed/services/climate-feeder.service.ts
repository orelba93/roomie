import { Injectable } from "@sugoi/server";
import { IFeeder } from "../interfaces/feeder.interface";
import {FeederTypes} from "../../../../../common/feed";
import { HttpService } from "../../core/services/http.service";
import { ClimateStatus } from "../../../../../common/climate";
import { ClimateStatusModel } from "../../climate/model/climate-status.model";
import { QueryOptions, SortItem, SortOptions } from "@sugoi/mongodb";
import { ValuesetService } from "../../valueset/services/valueset.service";

@Injectable()
export class ClimateFeederService implements IFeeder<any> {
    private static readonly REQUERY_HOURS_LIMIT: number    =   .5;
    private static readonly BASE_URL: string               =   'http://dataservice.accuweather.com/';
    private static readonly API_KEY: string                =   'lMJIIsw5idj6ioPmtAeSRgaYLlpBWkVN';
    public type: FeederTypes                               =   FeederTypes.CLIMATE;
    private cacheMap                                       =   new Map<string, Promise<ClimateStatusModel>>();



    constructor(private _http: HttpService, private _valuesetService: ValuesetService){
    }

    setType(type: FeederTypes): IFeeder<any> {
        this.type = type; 
        return this;
    }
    
    async query(payload?: {country: string, city: string, state?: string}): Promise<{ 
                                    data: ClimateStatus, 
                                    type: FeederTypes,
                                    corrolationId: string
                                }> {
        const countriesMap = await this._valuesetService.getValueset('countries');
        const countryCode = countriesMap[payload.country.toLowerCase()];
        if(!(countryCode && payload.city)){
            return null;
        }
        const cacheKey = `WEATHER_${payload.city}_${countryCode}`.toUpperCase();
        let climate: ClimateStatusModel;
        if(this.cacheMap.has(cacheKey)){
            climate = await this.cacheMap.get(cacheKey);
        }
        else{
            climate = await ClimateStatusModel.findOne({roomId: cacheKey}, QueryOptions.builder().setSortOptions(new SortItem(SortOptions.DESC, 'meta.lastUpdated')))
                                                .catch(err =>{
                                                    console.error(err);
                                                    return null;
                                                });
        }
        if(!(climate 
            && Math.floor((new Date().getTime() - new Date(climate.meta.lastUpdated).getTime())/1000/60/60) < ClimateFeederService.REQUERY_HOURS_LIMIT)){
            try{
                const cityCode = await this._http.get(ClimateFeederService.BASE_URL+`/locations/v1/cities/${countryCode}/search?q=${payload.city}&apikey=${ClimateFeederService.API_KEY}`);
                const locationKey = cityCode.data[0].Key;
                const promise = this._http.get(ClimateFeederService.BASE_URL+'currentconditions/v1/'+locationKey+'?apikey='+ClimateFeederService.API_KEY)
                .then(response=>{
                        const data = response.data[0];
                        return ClimateStatusModel.of({
                                label: payload.city, //location..name
                                temperature: data['Temperature'].Metric.Value,
                                state: data['WeatherText'],
                                roomId: cacheKey,
                                city: payload.city,
                                country: payload.country
                        }).save()
                    })
                this.cacheMap.set(cacheKey, promise)
                climate = await promise;
            }catch (e){
                climate = ClimateStatusModel.of({
                        label: payload.city, //location..name
                        temperature: null,
                        state: null,
                        roomId: cacheKey,
                        city: payload.city,
                        country: payload.country
                });
            }
        }
        
        return {type: this.type, data: climate, corrolationId: ''};
        
    }



}