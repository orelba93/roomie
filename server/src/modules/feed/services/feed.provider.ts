import { Injectable, Injector } from "@sugoi/server";
import { IFeeder } from "../interfaces/feeder.interface";
import { FeederTypes } from "../../../../../common/feed";
import { ClimateFeederService } from "./climate-feeder.service";
import { DefaultFeeder } from "./default-feeder.service";
import { IOTClimateFeederService } from "./iot-climate-feeder.service";

@Injectable()
export class FeedProvider {

    constructor(private injector: Injector){
    }

    getFeeder<T = any>(type: FeederTypes): IFeeder<T>{
        switch(type.toUpperCase()){
            case FeederTypes.IOT_CLIMATE:
                return this.injector.get<IFeeder<any>>(IOTClimateFeederService);
            case FeederTypes.CLIMATE:
                return this.injector.get<IFeeder<any>>(ClimateFeederService);
            default:
                return this.injector.get<IFeeder<any>>(DefaultFeeder).setType(type)

        }
    }


}

