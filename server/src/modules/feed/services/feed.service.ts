import { Injectable, Inject } from "@sugoi/server";
import { RoomModel } from "../../authorization/models/room.model";
import { FeedProvider } from "./feed.provider";
import { SocketOn, SocketHandler } from "@sugoi/socket";
import { SocketMessage } from '../../../../../common/core/classes/socket-message.class';

@Injectable()
export class FeedService {
    

    @Inject(FeedProvider)
    private feedProvider: FeedProvider;

    constructor(){
    }

    @SocketOn('subscribe')
    private  registerToUpdates(socket: SocketIO.Socket, msg: SocketMessage<{subjects: string[]}>){
        socket.join(msg.data.subjects, (err)=>{
            if(err){
                console.error('[subscribe] err', err)
            }
        })
    }


    public async getFeed(userId: string , roomId: string){
        const rooms = roomId ? await RoomModel.findById(roomId)
        .then(room => room.authorization.find(auth => auth.reference === userId) ? [room] : [])
        .catch(e => [])
        : await RoomModel.findByAuthorization(userId).catch(_ => []);
        
        const promiseArr = rooms.map( async room => this.getFeedForRoom(room));

        try {
            return await Promise.all(promiseArr).then(feeds => ({feeds: feeds.filter(feed => !!feed)}));
        } catch (error) {
            console.error(error);
            return {feeds:[]};
        }
    }


    async getFeedForRoom(room: RoomModel) {
        let feed$;
            if(room && Array.isArray(room.features)){
                feed$ = room.features.map( feat =>{
                    feat.payload = feat.payload || {};
                    feat.payload.roomId = room.id;
                    const feeder = this.feedProvider.getFeeder(feat.type as any);
                    return (feeder 
                        ? feeder.query(feat.payload)
                        .then(feed => {
                            if(!feed){
                                return null;
                            }
                            feed.corrolationId = feat.id;
                            return feed;
                        })
                        .catch(e => {
                            console.error(e);
                            return null;
                        }) 
                        : Promise.resolve(null));
                });
            }else{
                feed$ = Promise.resolve([]);
            }
            room['feed'] = await Promise.all(feed$).catch( e =>{
                console.error(e)
                return [];
            });
            room['feed'] = room['feed'].filter(feed => !!feed)
            return room;
    }

}