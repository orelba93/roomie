import { ServerModule } from "@sugoi/server"
import { FeedController } from "./controllers/feed.controller";
import { FeedService } from "./services/feed.service";
import { FeedProvider } from "./services/feed.provider";
import { SocketHandler } from "@sugoi/socket";
import { serverInstance } from "../../app/server";
import { ClimateFeederService } from "./services/climate-feeder.service";
import { IOTClimateFeederService } from "./services/iot-climate-feeder.service";
import { CoreModule } from "../core/core.module";
import { DefaultFeeder } from "./services/default-feeder.service";

@ServerModule({
    modules:[CoreModule],
    controllers:[FeedController],
    services:[
        FeedProvider,
        FeedService,
        DefaultFeeder,
        ClimateFeederService, 
        IOTClimateFeederService,
        {
            provide: () => SocketHandler.init(serverInstance),
            useName: 'SocketHandler'
        }
    ]
})
export class FeedModule{}