import { ServerModule } from "@sugoi/server"
import { ValuesetController } from "./controllers/valueset.controller";
import { ValuesetService } from "./services/valueset.service";

@ServerModule({
    modules:[],
    controllers:[ValuesetController],
    services:[ValuesetService]
})
export class ValuesetModule{}