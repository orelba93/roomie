import {
    Controller,
    HttpGet,
    RequestParam
 } from "@sugoi/server";
import { ValuesetService } from "../services/valueset.service";

@Controller('/valueset')
export class ValuesetController {

    constructor(
        private valuesetService:ValuesetService
    ){
    }


    @HttpGet("/:type")
    async index(@RequestParam("type") valuesetType: string){
        return await this.valuesetService.getValueset(valuesetType);
    }

}