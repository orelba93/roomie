import { Injectable } from "@sugoi/server";
import { Countries } from "../constants/countries.constant";

@Injectable()
export class ValuesetService {

    constructor(){
    }

    async getValueset(valuesetType: string): Promise<any>{
        switch (valuesetType && valuesetType.toLowerCase()) {
            case 'countries':
                return Countries;
            default:
                return [];
        }
    }


}