import {
    Controller,
    HttpGet,
    RequestParamsSchemaPolicy,
    Cookies,
    HttpPost,
    RequestBody,
    HttpPut,
    RequestParam,
    SchemaTypes,
    HttpDelete,
    ComparableSchema,
    HttpResponse
 } from "@sugoi/server";
import { UserService } from "../services/user.service";
import { RoomModel } from "../../authorization/models/room.model";
import { FeedService } from "../../feed/services/feed.service";

@Controller('/user')
// @Authorized()
export class UserController {

    constructor(
        private feedService: FeedService,
        private userService:UserService
    ){
    }


    @HttpGet("/")
    async getUserData(@Cookies('roomie-at') id: string){
        return await Promise.all([
            this.userService.getUserData(id),
            RoomModel.findByAuthorization(id).catch(_ => [])
        ]).then(([user,rooms]) => {
            return {
                user,
                rooms
            }
        })
    }

    @HttpPost('/room')
    async createRoom(
        @RequestBody() body: RoomModel, 
        @Cookies('roomie-at') userId: string){
        const room = new RoomModel(body.name, body.type, body.features);
        room.addRoomToUser(userId);
        return await room.save().then(_ => RoomModel.findByAuthorization(userId));

    }

    @HttpPut('/room/:id')
    async updateRoom(
        @RequestBody() body: RoomModel, 
        @RequestParam('id') roomId: string,
        @Cookies('roomie-at') userId: string){
        const room = await RoomModel.findById(roomId);
        
        if(!room.authorization.find(auth => auth.reference === userId)){
            return 403;
        }
        let updatedRoom
        try{
            updatedRoom = await room.patch({name: body.name, features: body.features});
        }catch(e){
            console.error('user put', e);
            return 500;
        }
        return await this.feedService.getFeedForRoom(updatedRoom);
    }

    @HttpDelete('/room/:id')
    async removeRoom(@RequestParam('id') id: string){
        try{
            return await RoomModel.removeById(id);
        }catch(e){
            console.error(e);
            return new HttpResponse('Error occur', null, 500);
        }
    }

}