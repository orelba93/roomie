import { BaseModel } from '../../core/models/base.model';
import { ModelName, Ignore } from "@sugoi/mongodb";

@ModelName('User')
export class User extends BaseModel{
    @Ignore()
    subscriptionsRefProperties: string[];
    static readonly PSK: string = '8YquK3_kJtyqb2p#%b3#Wn=#MT5XVH3@R';
    public passwordHash: string;
    public type: string;
    public identifier: string;
    public identifierType: string;
    public firstName: string;
    public lastName: string;
    public picture: string;

    static async register(
        identifier: string, 
        type: string,
        payload?: any,
        identifierType: "email" = "email"
        ){
           const user = new this(identifier, type); 
           switch(identifierType){
               case 'email':
                   if(type !== 'external'){
                    await user.setPassword(payload as string)
                   }
                   break;
           }
           return user;
    }


    constructor(identifier: string, type: string){
        super()
        this.identifier = identifier;
        this.type = type;
    }
    async setPassword(password: string) {
        
    }
}