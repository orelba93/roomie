import { ServerModule } from "@sugoi/server"
import { UserController } from "./controllers/user.controller";
import { UserService } from "./services/user.service";

@ServerModule({
    modules:[],
    controllers:[UserController],
    services:[UserService]
})
export class UserModule{}