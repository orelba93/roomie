import { Injectable } from "@sugoi/server";
import { User } from "../models/user.model";

@Injectable()
export class UserService {
    constructor(){
    }


    async getUserData(id: string): Promise<User> {
        return await User.findById(id)
        .then((user) => user)
        .catch(() => null);
    }


}