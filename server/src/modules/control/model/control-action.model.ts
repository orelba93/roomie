import { MongoModel, ModelName, IBeforeSave, IBeforeUpdate, IValidate } from "@sugoi/mongodb";
import {IR_GENERIC_ACTIONS} from "../../../../../common/control"

@ModelName('ControlAction')
export class ControlActionModel extends MongoModel implements IBeforeSave, IBeforeUpdate, IValidate {
    protected unit: string = "celcius"

    constructor(
        public room: string,
        public action: IR_GENERIC_ACTIONS,
        public temperature?: number,
    ){
        super();
    }

    validate(): Promise<any> {
        return Promise.resolve(this.action && this.room);
    }

    beforeUpdate(): void | Promise<any> {
        this.prepareData();
    }
    
    beforeSave(): void | Promise<any> {
        this.prepareData();
    }

    private prepareData(){
        this.action = this.action.toUpperCase() as any;
    }
}