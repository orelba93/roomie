import {
    Controller,
    HttpGet,
    RequestParam
 } from "@sugoi/server";
import { ControlService } from "../services/control.service";

@Controller('/control')
export class ControlController {

    constructor(
        private controlService: ControlService
    ){
    }


    @HttpGet("/:roomId/:type")
    async index(
            @RequestParam("roomId") roomId: string,
            @RequestParam("type") type: string
        ){
            return {"message": "DONE"}
        }

}