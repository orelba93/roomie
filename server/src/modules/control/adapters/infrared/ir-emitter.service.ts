import { Injectable, Inject } from "@sugoi/core";
import { Injector } from "@sugoi/server";
import { IR_GENERIC_ACTIONS } from "../../../../../../common/control";
import { SocketIOStatic } from "@sugoi/socket";
import { SocketMessage } from '../../../../../../common/core/classes/socket-message.class'

@Injectable()
export class IREmitterService {

    @Inject('ControlSocket')
    private controlEmitter: SocketIOStatic.Server


    constructor(private injector: Injector){}

    protected handleSingal(data): string[] {
        let commands = data.toString().split('\n');
        return commands.map(command => command.split(' ').pop()).filter(command => !!command);
    }

    protected async eventEmitter(roomId: string, action:string,interfaceName:string="",command:string=""): Promise<any> {
            console.log(`lirc command sent to - ${interfaceName} command - ${command}`);
            this.controlEmitter.to(roomId).emit(IR_GENERIC_ACTIONS.IR_EVENT,
                new SocketMessage('CONTROL_EVENT',["irsend", action, interfaceName, command])
            )
            // let child = spawn("irsend", [action, interfaceName, command]);
            
            // child.stdout.on('data', data => {
            //     console.log("success " + data);
            //     resolve(this.handleSingal(data));
            // });
            
            // child.stderr.on('data', data => {
            //     console.log("error " + data);
            //     reject(this.handleSingal(data));
            // });
            
            // child.on('close', (code) => {
            //     console.log("closed " + code);
            //     resolve(code);
            // });
        }

    // public getCommands(roomId: string, interfaceName: string = ""): Promise<any> {
        // return this.eventEmitter("list", interfaceName, "");
    // }

    public sendCommand(roomId: string, interfaceName: string, command: string): Promise<any> {
        console.log("sending ir command: %s | to interface: %s",command,interfaceName)
        return this.eventEmitter("send_once", interfaceName, command);
    }
}