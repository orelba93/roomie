import { ServerModule } from "@sugoi/server"
import { ControlController } from "./controllers/control.controller";
import { ControlService } from "./services/control.service";
import { IREmitterService } from "./adapters/infrared/ir-emitter.service";
import { SocketHandler } from "@sugoi/socket";
import { serverInstance } from "../../app/server";

@ServerModule({
    modules:[],
    controllers:[ControlController],
    services:[
        {
            provide: () => SocketHandler.init(serverInstance, {}, '/CONTROL'),
            useName: "ControlSocket"
        },
        ControlService,
        IREmitterService
    ]
})
export class ControlModule{}