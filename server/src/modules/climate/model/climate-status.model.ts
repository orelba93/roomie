import { ClimateStatus, ClimateSocketEvents } from '../../../../../common/climate'
import { BaseModel } from "../../core/models/base.model";
import { IAfterSave, ModelName } from '@sugoi/orm';
import { SocketIOStatic, SocketHandler } from '@sugoi/socket';
import { Container } from '@sugoi/server';
import { server } from '../../../app/app';

@ModelName('ClimateStatus')
export class ClimateStatusModel extends BaseModel implements ClimateStatus, IAfterSave{
    subscriptionsRefProperties: string[] = ['payload.city', 'payload.county'];
    
    private get climateSocketHandler(): SocketIOStatic.Server{
        return (<Container>server.container).get<SocketIOStatic.Server>('ClimateSocket');
    }

    constructor(
        public label: string,
        public temperature: number,
        public country?: string,
        public city?: string,
        public state?: string,
        public humidity?: number,
        public roomId?: string,
        public timestamp: number = new Date().getTime(),
        
    ){
        super();
    }

    static of(data: ClimateStatus){
        return new this(data.label, data.temperature,data.country, data.city, data.state, data.humidity, data.roomId);
    }

    // FOR DEBUG PURPOSE ONLY
    // saveEmitter(options:any){
    //     console.log(this);
    //     return Promise.resolve(this);
    // }

    afterSave(saveResponse?: any): void {
        // this.redisHandler.hset(saveResponse.roomId, saveResponse);
        this.climateSocketHandler.emit(ClimateSocketEvents.TEMPERATURE_EVENT, saveResponse)
    }

}