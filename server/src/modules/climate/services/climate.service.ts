import {Injectable, OnEvent} from "@sugoi/core";
import {SocketOn, SocketIOStatic} from "@sugoi/socket";
import { EachBatchPayload } from "kafkajs";
import {IR_GENERIC_ACTIONS} from "../../../../../common/control";
import {ClimateSocketEvents, ClimateStatus} from "../../../../../common/climate";
import { IREmitterService } from "../../control/adapters/infrared/ir-emitter.service";
import { Injector } from "@sugoi/server";
import { TRedisProvider } from "@sugoi/redis";
import { ControlActionModel } from "../../control/model/control-action.model";
import { ClimateStatusModel } from "../model/climate-status.model";
import { KafkaService } from "../../core/services/kafka.service";


@Injectable()
export class ClimateService {
    private static readonly TEMPERATURE_EVENT = ClimateSocketEvents.TEMPERATURE_EVENT;
//    private redisClimates: RedisClient = new RedisModel(redisEnum.iot, RedisConnectionTypes.QUERY);
    private get redisClimates(): TRedisProvider{
        return this.injector.get<TRedisProvider>('ClimateRedis');
    }

    private get climateSocketHandler(): SocketIOStatic.Server{
        return this.injector.get<SocketIOStatic.Server>('ClimateSocket');
    }

    constructor(
        private _IREmitter: IREmitterService, 
        private injector: Injector,
        private _kafkaService: KafkaService
    ) {
        this._kafkaService.consume('roomie', 'IOT','IOT_CLIMATE');
    }

    public turnOn(roomId: string) {
        return this._IREmitter.sendCommand(roomId, 'ac-full', IR_GENERIC_ACTIONS.POWER_ON);
    }

    public turnOff(roomId: string) {
        return this._IREmitter.sendCommand(roomId, 'ac-full', IR_GENERIC_ACTIONS.POWER_OFF);
    }

    public async getTemperature(room: string = 'room1') {
            let data = await this.redisClimates.hgetall(room);
            console.log(data);
            return data;
    }

    public setMode(roomId: string, mode: IR_GENERIC_ACTIONS, temperature: number, swing: number) {
        let action = swing == 1 ? `KEY_${mode.toUpperCase()}_${temperature}_SWING` : `KEY_${mode.toUpperCase()}_${temperature}`;
        return this._IREmitter.sendCommand(roomId, 'ac-full', action)
            .then(res => {
                this.updateACRecord(mode, temperature);
                return res;
            });
    }

    @SocketOn(ClimateService.TEMPERATURE_EVENT, '/UPDATOR')
    public subscribeToClimateChange(socket: SocketIOStatic.Socket, data: ClimateStatus ) {
        socket.server.to(data.roomId).emit(ClimateService.TEMPERATURE_EVENT, data);
        this.storeClimate(data);
    } 
    
    storeClimate(data: any) {
        ClimateStatusModel.of(data).save()
    }

    @SocketOn('connect')
    connect(socket, message){
        console.log('connect',socket.id)
    }

    @OnEvent('IOT_CLIMATE', false, KafkaService.KAFKA_CHANNEL)
    private async handlerIOTClimateEvent(data: EachBatchPayload){
        const promises = data.batch.messages.map(msg =>{ 
            try{
                const value = JSON.parse(msg.value.toString('utf8'));
                const cs = new ClimateStatusModel(
                    "",
                    value['temperature']
                );
                cs.timestamp = parseInt(msg.timestamp);
                cs.roomId = msg.headers['room_id'].toString('utf8');
                cs.humidity = value['humidity'];
                return cs.save();
            }catch(e){
                console.error(e);
                return null;
            }
        })
        await Promise.all(promises);
        data.resolveOffset(data.batch.messages.pop().offset);
    }



    private async updateACRecord(action: IR_GENERIC_ACTIONS,
                                 temperature: number = null,
                                 room?: string): Promise<ControlActionModel> {
        return await this.getTemperature(room)
            .then((status) => {
                return new ControlActionModel(room, action, temperature).save();
            });
    }

    @SocketOn('client-data')
    public eventCheck(data) {
        console.log(data)
    }
}