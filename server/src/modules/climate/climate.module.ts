import { ServerModule, Inject, Injector } from "@sugoi/server"
import { ClimateController } from "./controllers/climate.controller";
import { ClimateService } from "./services/climate.service";
import { SocketHandler, SocketIOStatic } from "@sugoi/socket";
import { serverInstance } from "../../app/server";
import { RedisProvider } from "@sugoi/redis";

@ServerModule({
    modules:[],
    controllers:[ClimateController],
    services:[
    {
        provide: () => SocketHandler.init(serverInstance, undefined , '/UPDATOR'),
        useName: "UpdatorSocket"
    },
    {
        provide: () => SocketHandler.init(serverInstance),
        useName: "ClimateSocket"
    },
    {
      provide: () => RedisProvider.GetConnection(),
      useName: "RedisHandler"
    },
    ClimateService
]
})
export class ClimateModule{
    constructor(private injector: Injector){
    }
}
