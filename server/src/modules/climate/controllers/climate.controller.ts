import {
    Controller,
    HttpGet,
    RequestParam
 } from "@sugoi/server";
import { ClimateService } from "../services/climate.service";

@Controller('/climate')
export class ClimateController {

    constructor(
        private climateService: ClimateService
    ){
    }


    @HttpGet("/:payload?")
    async index(@RequestParam("payload") payload:string){
        const responseMsg = "climate is ready!";
        return payload
                ? `${responseMsg} got payload ${payload}`
                : `${responseMsg}`;
    }

}