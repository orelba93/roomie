import { Authorization } from "../classes/authorization.class";
import * as passport from 'passport';

export class PassportAuthorization extends Authorization {
    static strategies: any[];
    
    static of(...strategies: any[]){
        this.strategies = strategies;
        return this;
    }

    async isAuthenticated(): Promise<boolean> {
        return await Promise.resolve(passport.use((<any> this.constructor).strategies));
    }

    /**
    * Retrieve the user and store it in cache for later use
    *
    * @returns {Promise<boolean>}
    */
    async getUser(req, res, next): Promise<any> {
        let userData = await this.getUserData();
        if(!userData){
            userData = await Promise.resolve(null).then(_userData=>{
                this.setUserData(_userData);
                return _userData;
            });
        }
        return userData;
    }

}
