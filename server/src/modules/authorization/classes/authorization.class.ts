import {AuthProvider,TStringOrNumber} from "@sugoi/server";

export class Authorization extends AuthProvider<any> {
    /**
     * Verify if the user is authorized
     *
     * @returns {Promise<boolean>}
     */
    async isAuthenticated(): Promise<boolean> {
        return !!this.details;
    }


    /**
    * Verify if the user has the right permissions
    *
    * @returns {Promise<boolean>}
    */
    isAllowedTo(...permissions: TStringOrNumber[]): Promise<boolean> {
            return Promise.resolve(null);
    }


    /**
    * Verify if the user is in the right role
    *
    * @returns {Promise<boolean>}
    */
    isInRole(...roles: TStringOrNumber[]): Promise<boolean> {
        return Promise.resolve(null);
    }


    /**
    * Retrieve the user and store it in cache for later use
    *
    * @returns {Promise<boolean>}
    */
    async getUser(req, res, next): Promise<any> {
        let userData = await this.getUserData();
        if(!userData){
            userData = req.cookie('roomie-at');
            this.setUserData(userData);
            return userData;
            
        }
        return userData;
    }




    isResourceOwner(resourceId: any): Promise<boolean> {
        return Promise.resolve(null);
    }

}
