import { BaseModel } from "../../core/models/base.model";
import { ModelName, IBeforeUpdate, IBeforeSave } from "@sugoi/mongodb";
import { ResourceContext } from "../../core/classes/resource-context.class";
import { FeederTypes, FeedSocketEvents } from "../../../../../common/feed";
import { server } from "../../../app/app";
import { FeedService } from "../../feed/services/feed.service";


@ModelName('Room')
export class RoomModel extends BaseModel implements IBeforeSave, IBeforeUpdate{
    private static get FeedService(): FeedService{
        return (<any>server.container).get(FeedService);
    }    
    subscriptionsRefProperties: string[] = ['id'];
    constructor(
        public name: string,
        public type: string,
        public features: Array<{type: string | FeederTypes, payload: any, id?: string, order: number}>
        
    ){
        super();
    }


    public addRoomToUser(...ids: string[]){
        this.setAuthroization(...this.authorization,...ids.map(id => new ResourceContext('User', id)));
    }

    
    public addRoomToGroup(...ids: string[]){
        this.setAuthroization(...this.authorization,...ids.map(id => new ResourceContext('Group', id)));
    }

    public updateFeature(featureId: string, feature: any) {
        const index = this.features.findIndex(feature => feature.id === featureId)
        if(index < 0){
            this.features.push(feature);
        }else{
            this.features[index] = feature;
        }
    }

    protected async triggerUpdate(){
        super.triggerUpdate(FeedSocketEvents.UPDATE_FEED, await RoomModel.FeedService.getFeedForRoom(this));
    }

    beforeSave(){
        super.beforeSave();
        delete this['feed'];
        this.normalize();
    }
    normalize() {
        if(!this.features){
            this.features = []
        }
        this.features.forEach((feature, index) =>{
                 feature.order = this.features.length;
                 feature.type = feature.type.toUpperCase()
                 feature.id = feature.id || index.toString();
        });
        this.features.sort((f1, f2) => f1['order'] - f2['order'])
    }

    beforeUpdate(){
        super.beforeUpdate();
        this.normalize();

    }
}