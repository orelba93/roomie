import { ModelName } from "@sugoi/orm";
import { BaseModel } from "../../core/models/base.model";

@ModelName('Group')
export class GroupModel extends BaseModel{
    subscriptionsRefProperties: string[];
    public owners: string[] = []; 
    public _members: string[] = [];
    get allMembers(){
        return [...this._members,... this.owners];
    }
}