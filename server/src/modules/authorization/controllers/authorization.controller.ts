import {
    Controller,
    HttpGet,
    Request,
    HttpPost,
    RequestBody,
    HttpResponse,
    Response,
    express
 } from "@sugoi/server";
import { AuthorizationService } from "../services/authorization.service";
import { User } from "../../user/models/user.model";
import * as crypt from "crypto";

@Controller('/authorization')
export class AuthorizationController {

    constructor(
        private authorizationService:AuthorizationService
    ){
    }


    @HttpPost("/registerExternal")
    async registerExternal(@RequestBody() user, @Response() res:express.Response){

        let users, userData;
        try{
            users = await User.find({identifier: user.email});
        }catch(e){
            console.log(e);
            users = [];
        }

        if(!(Array.isArray(users) && users.length > 0) ){
            const storeUser = await User.register(user.email, 'google');
            storeUser.firstName = user.firstName;
            storeUser.lastName = user.lastName;
            storeUser.picture = user.picture;
            userData = await storeUser.save();
        }else{
            userData = users[0];
        }

        res.cookie('roomie-at', userData.getMongoId(), {
            httpOnly: false
        });//this.authorizationService.encrypt(userData.id))
        return new HttpResponse(userData,{
            'xsrf': '1'//this.authorizationService.encrypt(new Date().getTime + userData.id).encryptedData
        },);
    }

}