import { ServerModule } from "@sugoi/server"
import { AuthorizationController } from "./controllers/authorization.controller";
import { AuthorizationService } from "./services/authorization.service";

@ServerModule({
    modules:[],
    controllers:[AuthorizationController],
    services:[AuthorizationService]
})
export class AuthorizationModule{}