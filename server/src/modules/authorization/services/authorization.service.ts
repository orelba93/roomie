import { Injectable } from "@sugoi/server";
import { Authorization } from "../classes/authorization.class";
import {createCipheriv, randomBytes, createDecipheriv} from "crypto";
@Injectable()
export class AuthorizationService{
    static key = randomBytes(32);
    static iv = randomBytes(16);
    constructor(){
    }

    encrypt(data: string){
        const cipher = createCipheriv('aes-256-ccm', Buffer.from(AuthorizationService.key), AuthorizationService.iv);
        let encrypted = cipher.update(data);
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        return { iv: AuthorizationService.iv.toString('hex'), encryptedData: encrypted.toString('hex') };
    }

    decrypt(data: string) {
        AuthorizationService.iv;
        let encryptedText = Buffer.from(data, 'hex');
        let decipher = createDecipheriv('aes-256-cbc', Buffer.from(AuthorizationService.key), AuthorizationService.iv);
        let decrypted = decipher.update(encryptedText);
        decrypted = Buffer.concat([decrypted, decipher.final()]);
        return decrypted.toString();
       }


}