import { ServerModule } from "@sugoi/server"
import { HttpService } from "./services/http.service";
import { KafkaService } from "./services/kafka.service";

@ServerModule({
    modules:[],
    controllers:[],
    services:[HttpService, KafkaService]
})
export class CoreModule{}