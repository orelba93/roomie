import { Injectable } from "@sugoi/server";
import { EventEmitter } from "@sugoi/core";
import { Kafka, Consumer,EachBatchPayload, Producer, Message, CompressionTypes } from 'kafkajs';



@Injectable()
export class KafkaService {
    public static readonly KAFKA_CHANNEL = 'KAFKA_CHANNEL';
    private _kafka: Kafka;
    private _emitter: any;

    constructor(){
        this._init();
    }

    private _init(){
        const KAFKA_SERVER = process.env.KAFKA_SERVER || '127.0.0.1:9092'
        console.log(KAFKA_SERVER)
        this._kafka = new Kafka({
            clientId: 'roomie',
            brokers: [...KAFKA_SERVER.split(',')],
            sasl: {
                mechanism: process.env.KAFKA_MECHANISM as any || "scram-sha-256",
                username: process.env.KAFKA_USER || "roomie",
                password: process.env.KAFKA_PASSWORD || "roomie",
            }
        });
        this._emitter = new EventEmitter(KafkaService.KAFKA_CHANNEL);
    }

    public registerToTopic(topic: string | RegExp, cb: (data: EachBatchPayload) => void){
        this._emitter.channel.on(topic, cb);
    }

    public async consume(groupId: string, ...topics: Array< string | RegExp >): Promise<Consumer>{
        const consumer = this._kafka.consumer({groupId, allowAutoTopicCreation: true});
        await this.registerTopics(consumer, ...topics);
        return consumer;
    }

    public async produce(topic: string, ...msgs: Array<Message>): Promise<Producer>{
        const producer = this._kafka.producer({
            allowAutoTopicCreation: true,
        });

        await producer.connect();
        await producer.send({
            messages: msgs,
            topic,
            compression: CompressionTypes.GZIP
        })
        return producer;

    }

    public async registerTopics(consumer: Consumer, ...topics: Array< string | RegExp>){
        await consumer.connect();
        await Promise.all(
            topics.map( topic => consumer.subscribe({ topic: topic, fromBeginning: true,  }) )
        );
        await consumer.run({
            eachBatch: async (event: EachBatchPayload) => {
                console.info('new kafka batch messages',event);
                this._emitter.emit(event.batch.topic, event)
            }
        })
    }



}