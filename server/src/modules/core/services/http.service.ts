import { Injectable } from "@sugoi/server";
import {AxiosResponse, AxiosRequestConfig, AxiosInstance} from "axios";
const axios = require("axios");

@Injectable()
export class HttpService{
    private _requester: AxiosInstance = axios.create();

    get<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R>{
        console.log(url)
        return this._requester.get(url, config);
    }
    delete<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R>{
        return this._requester.delete(url, config);
    }
    head<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R>{
        return this._requester.head(url, config);
    }
    post<T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R>{
        return this._requester.post(url, data, config);
    }
    put<T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R>{
        return this._requester.put(url, data, config);
    }
    patch<T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R>{
        return this._requester.patch(url, data, config);
    }



}