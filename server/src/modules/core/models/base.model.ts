import { MongoModel, IBeforeUpdate, IBeforeSave, QueryOptions, IAfterSave, IAfterUpdate, Ignore } from "@sugoi/mongodb";
import { ResourceContext } from "../classes/resource-context.class";
import { server } from "../../../app/app";
import { SocketIOStatic, SocketHandler } from "@sugoi/socket";
import { SocketMessage } from "../../../../../common/core/classes/socket-message.class";
import { SocketMeta } from "../../../../../common/core/classes/socket-meta.class";
import { get } from "lodash";

export abstract class BaseModel extends MongoModel implements IBeforeSave, IBeforeUpdate, IAfterSave, IAfterUpdate{
    
    @Ignore()
    abstract subscriptionsRefProperties: string[]

    id: string;
    meta: {
        lastUpdated: string;
        metaId : string,
        created: string
    };
    authorization: ResourceContext[] = [];
    context: ResourceContext[];

    public static get SocketHandler(): SocketIOStatic.Server{
        return (<any> server.container).get('SocketHandler');
    }

    beforeSave(): void | Promise<any> {
        this.setMeta(true);
    }

    beforeUpdate(): void | Promise<any> {
        this.setMeta(false)
    }
    
    afterUpdate(updateResponse?: any): void | Promise<any> {
        this.triggerUpdate();
    }
    afterSave(saveResponse?: any): void | Promise<any> {
        this.triggerUpdate();
    }

    public patch(data: any, options?: QueryOptions, query?: any ){
        delete data.context;
        delete data.authorization;

        Object.assign(this, data);
        return this.update(options, query);
    }

    public setContext(...context: Array<ResourceContext>){
        this.context = context;
    }

    public setAuthroization(...authorization: Array<ResourceContext>){
        this.authorization = authorization;
    }

    public static async findByAuthorization(...ids: Array<string>){
        return await this.find({"authorization.reference": {"$in":ids}});
    }

    public static async findByContext(query: {ids: Array<string>, options?: QueryOptions}){
        return await this.find({"context.reference": {"$in":query.ids}}, query.options);
    }

    private setMeta(creation: boolean){
        this.meta = this.meta || {} as any;
        if(creation){
            this.meta.created = new Date().toISOString()
        }
        this.meta.lastUpdated = new Date().toISOString()
        this.meta.metaId = new Date().getTime().toString()
    }

    protected async triggerUpdate(event?, data?){
        if(!event || !data){
            return;
        }
        if(!(this.subscriptionsRefProperties)){
            return;
        }else{
            
            const socketMessage = new  SocketMessage('UPDATE', data, new SocketMeta());
            this.subscriptionsRefProperties.length > 0
            ? this.subscriptionsRefProperties.forEach( ref => BaseModel.SocketHandler.to(get(this, ref, '').toString()).emit(`${event}`, socketMessage))
            : BaseModel.SocketHandler.emit(event, socketMessage);
        }
    }
}