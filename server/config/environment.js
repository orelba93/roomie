
const ENV = process.argv || ['development'];


module.exports = {
	ENV,
	isTest: ENV.indexOf('test') !== -1,
	isProd: ENV.indexOf('prod') !== -1,
	isTestWatch: ENV.indexOf('test') !== -1 && ENV.indexOf('watch') !== -1,
	isDev: !this.isTest && !this.isTestWatch && this.isProd === false,
};
