const path = require('path');
const WebpackWatchedGlobEntries = require('webpack-watched-glob-entries-plugin');


const _root = path.resolve(__dirname, '../../');

function root(args) {
  args = Array.prototype.slice.call(arguments, 0);
  return path.join.apply(path, [_root].concat(args));
}

function submodules() {
    return root('src','**/*.module.ts')
}

function modules(...paths) {
    return WebpackWatchedGlobEntries.getEntries(paths);
}

exports.root = root;
exports.modules = modules;
exports.submodules = submodules;
