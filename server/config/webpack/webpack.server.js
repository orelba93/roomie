const mergeWebpack = require('webpack-merge');
const nodeExternals = require('webpack-node-externals');
const commonConfig = require('./webpack.common');
const helpers = require('./webpack.helpers');
const env = require('../environment');


const config = {
    target: 'node',
    externals: [nodeExternals()],
    node: false,
    //     {
    //     __dirname: false,
    //     __filename: false,
    //     console: false,
    //     global: false,
    //     process: false,
    //     Buffer: false,
    //     setImmediate: false
    // },
    entry: helpers.modules(
        helpers.root('src', 'app/server.ts'),
        helpers.submodules()
    ),
    output: env.isTest ? {} : {
        filename: '[name].js',
        chunkFilename: '[name].chunk.js',
        publicPath: "./dist",
        path: helpers.root('dist')
    }
};


module.exports = mergeWebpack(commonConfig, config);
