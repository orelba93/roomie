const webpack = require('webpack');
const helpers = require('./webpack.helpers');
const CircularDependencyPlugin = require('circular-dependency-plugin');
const env = require('../environment');
const ENV = env.ENV;
const config = {};
const moduleRegex = /([^//\/]*.\.module)(\.ts)?/i;

config.mode = "development";
if (env.isProd) {
    config.devtool = false;
    config.mode = "production";
}
else if (env.isTest) {
    config.devtool = 'inline-source-map';
}
else {
    config.devtool = 'eval-source-map';
}

config.optimization = {
    minimize: false,
    namedChunks: true,
    sideEffects: true,
    occurrenceOrder: false,
    splitChunks:{
        automaticNameDelimiter: '.',
        chunks: 'async',
        minSize: 0,
        minChunks: Infinity
    }
};

config.resolve = {
    extensions: ['.ts', '.js', '.json'],
    modules: [
        helpers.root(__dirname)
    ]
};


config.module = {
    rules: [
        {
            test: /\.ts$/,
            exclude: [/node_modules/],
            loader: 'awesome-typescript-loader'
        }
    ]
};


config.plugins = [
    new webpack.BannerPlugin({
        banner: 'require("source-map-support").install();', raw: true, entryOnly: false
    }),
    new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(ENV)
    }),
    new CircularDependencyPlugin({
        // exclude detection of files based on a RegExp
        exclude: /node_modules/,
        // add errors to webpack instead of warnings
        failOnError: false,
        // set the current working directory for displaying module paths
        cwd: process.cwd()
    })
];


module.exports = config;