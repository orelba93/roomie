module.exports = {
    MONGODB: {
        hostName: 'cluster0-rnpqi.mongodb.net',
        protocol: 'mongodb+srv://',
        password: "17Ba0393",
        user: 'roomie-admin',
        db: 'test',
        authDB: 'admin',
        newParser: true,
        port:null,
        additionalConfig: {
            retryWrites: true,
            w: 'majority'
        }
    },
    REDIS:{
	    hostName:"roomie-redis"
    }
}
