package utils
import (
	"strings"
	"net/http"
)

func filterMethod(r *http.Request, method string) bool {
	return strings.ToUpper(r.Method) == strings.ToUpper(method)
}