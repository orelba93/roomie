package producer

import (
	"fmt"

	"gopkg.in/confluentinc/confluent-kafka-go.v1/kafka"
)

type EventProducer struct {
	producer kafka.Producer
}

func NewProducer() *EventProducer {
	p, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": "localhost"})

	if err != nil {
		panic(err)
	}

	defer p.Close()
	// Delivery report handler for produced messages
	go EmitEvents(p)
	producer := new(EventProducer)
	producer.producer = *p
	return producer
}

func EmitEvents(p *kafka.Producer) {
	for e := range p.Events() {
		switch ev := e.(type) {
		case *kafka.Message:
			if ev.TopicPartition.Error != nil {
				fmt.Printf("Delivery failed: %v\n", ev.TopicPartition)
			} else {
				fmt.Printf("Delivered message to %v\n", ev.TopicPartition)
			}
		}
	}
}

func (p *EventProducer) Emit(message string, topic string, headers ...kafka.Header) {

	var messageToSend kafka.Message
	messageToSend = kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
		Value:          []byte(message),
	}
	if headers != nil {
		messageToSend.Headers = headers
	}
	// Produce messages to topic (asynchronously)
	fmt.Printf("MSG %s", message)
	p.producer.Produce(&messageToSend, nil)

	p.producer.Flush(1000 * 15)
}
