package model

type ResourceContext struct {
	reference    string
	resourceType string
}
