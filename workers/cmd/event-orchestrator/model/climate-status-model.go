package model

type ClimateStatus struct {
	*ABaseModel
	label       string
	temperature float32
	humidity    float32
	country     string
	city        string
	state       string
	roomId      string
	timestamp   int
}

func NewClimateStatus(temperature float32, roomId string) ClimateStatus {
	cs := ClimateStatus{temperature: temperature, roomId: roomId}
	return cs
}
