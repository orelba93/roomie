package model

import (
	"context"
	"fmt"
	"log"
	"reflect"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Meta struct {
	lastUpdated int64
	metaId      int64
	created     int64
}

type ABaseModel struct {
	id            string
	meta          Meta
	authorization []ResourceContext
	context       []ResourceContext
}

func connect() *mongo.Client {
	fmt.Println("initiate mongo connection")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(
		"mongodb+srv://roomie-admin:17Ba0393@cluster0.rnpqi.mongodb.net/admin?retryWrites=true&w=majority",
	))
	if err != nil {
		log.Fatal(err)
	}
	return client
}

func getName(m *ABaseModel) string {
	t := reflect.TypeOf(m)
	for t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	return t.Name()
}

func NewABaseModel() ABaseModel {
	bm := ABaseModel{}
	now := time.Now().UnixNano() / int64(time.Millisecond)
	bm.meta = Meta{
		lastUpdated: now,
		created:     now,
		metaId:      now,
	}
	return bm
}

func Save(m Element) *ABaseModel {
	client := connect()
	fmt.Println(getName(m))
	panic("HI")
	collection := client.Database("test").Collection(getName(m))
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	fmt.Println(m)
	data := bson.M{"d": m}
	fmt.Println(m)
	fmt.Println(data)
	// if marshalErr != nil { panic(marshalErr)}
	res, err := collection.InsertOne(ctx, data)
	if err != nil {
		panic(err)
	}
	id := res.InsertedID
	fmt.Println(id)
	// m.id = string(id)
	return m
}

// func (m *ABaseModel) find(query){
//     return m
// }
