package main

import (
	"fmt"
	"os"

	"gopkg.in/confluentinc/confluent-kafka-go.v1/kafka"
	// producer "roomie/internal/utils/"
	model "events/model"
)

func consume() {
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": os.Getenv("KAFKA_SERVER"),
		"group.id":          os.Getenv("KAFKA_GROUP"),
		"auto.offset.reset": "latest",
	})

	if err != nil {
		panic(err)
	}

	c.SubscribeTopics([]string{"IOT"}, nil)

	for {
		msg, err := c.ReadMessage(-1)
		if err == nil {

			fmt.Printf("Message on %s: %s\n", msg.TopicPartition, string(msg.Value))
			fmt.Printf("Message header %s: %s\n", msg.TopicPartition, string(msg.Headers[0].Value))
		} else {
			// The client will automatically try to recover from all errors.
			model.NewClimateStatus(30.0, "123")
			fmt.Printf("Consumer error: %v (%v)\n", err, msg)
		}
	}
	c.Close()
}

// func prepSend(method string, ep *producer.EventProducer) func(w http.ResponseWriter, r *http.Request) {
// 	greet := func(w http.ResponseWriter, r *http.Request) {
// 		if !filterMethod(r, method) {
// 			return
// 		}

// 		fmt.Fprintf(w, "Hello World! %s", time.Now())
// 	}
// 	return greet
// }

// func initServer() {
// 	ep := producer.NewProducer()
// 	http.HandleFunc("/", prepSend("POST", ep))
// 	PORT := "8080"
// 	log.Fatal(http.ListenAndServe(":"+PORT, nil))
// 	fmt.Println("listening to port", PORT)
// }

func main() {
	c := model.NewClimateStatus(30.0, "123")
	model.Save(c)
	consume()
	// initServer()
}
