module events

go 1.15

require (
	github.com/confluentinc/confluent-kafka-go v1.5.2 // indirect
	github.com/fatih/structs v1.1.0
	go.mongodb.org/mongo-driver v1.4.3
	gopkg.in/confluentinc/confluent-kafka-go.v1 v1.5.2
)
