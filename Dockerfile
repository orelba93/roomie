# Install node v10
FROM node:12-slim as build_img

RUN npm i -g @sugoi/cli
RUN npm i -g @vue/cli-service

WORKDIR /roomie
COPY ./package.json ./


WORKDIR /roomie/common
COPY common/package.json ./
COPY common/ ./
RUN npm install

WORKDIR /roomie/client/roomie
COPY client/roomie/package.json ./
COPY client/roomie/ ./
RUN npm install
RUN npm run build

WORKDIR /roomie/server
COPY server/package.json ./
COPY server/ ./
RUN npm install
RUN npm run build:docker

FROM build_img as deploy_img
# Copy .env.docker to workdir/.env - use the docker env
# COPY .env.docker ./.env

EXPOSE 443 443 
EXPOSE 80 80

WORKDIR /roomie/server

CMD ["node", "dist/server.js"]
