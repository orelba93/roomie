export class Card{
    public type: string
    constructor(
        public corrolationId: string,
        type: CardTypes,
        public title: string,
        public payload: any = {}
    ){
        this.type = type.toLowerCase();
    }
}

export enum CardTypes{
    CLIMATE = 'climate',
    CONTROL = 'control',
    MAP = 'map',
    REMINDER = 'reminder',
    LIST = 'list'
}