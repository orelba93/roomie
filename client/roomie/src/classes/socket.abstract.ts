import * as io from 'socket.io-client';
import * as SocketIOStatic from 'socket.io';


export abstract class SocketAbstract {
protected static SOCKET_DEFAULT_NAMESPACE = '/';
protected static initMap: Map <string, boolean> = new Map();
protected events: Array<{ name: string, callback: ( data: any ) => void }> = [];
protected socket: SocketIOStatic.Socket | any;

  public setEvents(events: [{ name: string; callback: (data: any) => void; }]) {
    this.events = events;
    if (this.socket && this.socket.connected) {
      this.registerEvents();
    }
  }

  public connect(namespaceUri: string = SocketAbstract.SOCKET_DEFAULT_NAMESPACE) {
    if (SocketAbstract.initMap.has(namespaceUri)) {
        this.socket = SocketAbstract.initMap.get(namespaceUri)
        if(this.socket.connected){
          this.registerEvents();
          this.onConnect();
        }
        else{
          this.socket.on('connect',() => {
            this.registerEvents();
            this.onConnect();
          });
        }
        return this;
    }
    this.socket = io.connect(`${window.location.origin}${namespaceUri}`);
    this.socket.on('connect', (e) => {
      console.log('connect',e)
      this.socket.emit('test', {});;
      this.registerEvents();
      this.onConnect();
    });
    this.socket.on('disconnect', this.onDisconnect);
    SocketAbstract.initMap.set(namespaceUri, this.socket);
    return this;
  }

  public disconnect() {
    this.socket.disconnect();
  }
  public registerEvents() {
    this.events.forEach((event) => {
      this.registerToEvent(event.name, event.callback);
    });
  }

  public registerToEvent(event: string, callback: (eventData: any) => void = () => {
  }) {
    if(this.socket)
      this.socket.on(event, callback);
  }

  public deregisterFromEvent(event: string){
    if(this.socket)
      this.socket.off(event);
  }

  public sendMessage(event: string, data: any) {
    this.socket.emit(event, data);
  }

  public abstract onConnect(): void;

  public abstract onDisconnect(): void;
}
