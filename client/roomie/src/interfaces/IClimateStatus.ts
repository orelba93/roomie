import { ClimateStatus } from '../../../../common/climate';
export interface IClimateStatus extends ClimateStatus {
  label: string;
  temperature: number;
  state: string;
  humidity?: number;
  roomId?: string;
  timestamp?: number;
}
