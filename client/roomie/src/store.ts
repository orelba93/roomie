import Vue from 'vue';
import Vuex, { Store } from 'vuex';
import { Card, CardTypes } from './classes/card.class';
import { IClimateStatus } from './interfaces/IClimateStatus';
import FeedService from './services/FeedService';
Vue.use(Vuex);
const feedService = new FeedService();


export interface IRoomieState{
  global: {
    loader: number,
    dark: boolean,
    drawerStatus: boolean
  },
  rooms:{
    [roomId: string]: IRoom
  },
  activeRoom: 0,
  user: IUser | {}
}

export interface IUser{
  ttl: number,
  email: string,
  firstName: string,
  lastName: string,
  id: string
  groups: string[]
  picture: string
}

export interface IRoom{
    authorization: any[];
    name:string;
    type:string;
    features:{type:"CLIMATE" | "CONTROL",payload:any}[];
    id: string;
}
export interface IControl{
  id: string;
  status: boolean;
  label: string;
  payload: any;
}

const state: IRoomieState = {
  activeRoom: 0,
  rooms:{

  },
  global:{
    loader: 0,
    dark: false,
    drawerStatus: false
  },
  user: {}
};

export default new Vuex.Store({
  state,
  mutations: {
    UPDATE_TEMPERATURE(currentState, payload: {roomId?: string} & any ){
      if(payload.roomId && !currentState.rooms[payload.roomId]){
          const dataToSet = {climate: payload, controls: [], label: ''};
          Vue.set(currentState.rooms, payload.roomId, dataToSet);
      } else if(payload.roomId){
        const dataToSet = {...currentState.rooms[payload.roomId],...{climate: payload}};
        Vue.set(currentState.rooms, payload.roomId, dataToSet);
      }else{
        Vue.set(currentState.global, 'climate', payload);
      }
    },
    UPDATE_ROOM(currentState, payload: {id: string} & any ){
      Vue.set(currentState.rooms, payload.id, payload.data);
    },
    SET_ROOM_ID(currentState, payload: {id: string} & any ){
      Vue.set(currentState, 'activeRoom', payload);
    },
    SET_USER(currentState, user){
      Vue.set(currentState, 'user', user);
    },
    SET_ROOMS(currentState, rooms: Array<IRoom & {id: string}>){
      const roomsState = {};
      rooms = rooms || [];
      rooms.forEach(room => {
        roomsState[room.id] = room
      })
      Vue.set(currentState, 'rooms', roomsState);
      feedService.connect();
    },
    SET_ROOM(currentState, room: IRoom & {id: string}){
      currentState.rooms[room.id] = room
      Vue.set(currentState, 'rooms', {...currentState.rooms});
    },
    SET_LOADER(currentState, loadersStatus: number){
      currentState.global.loader += loadersStatus;
    },
    SET_DRAWER_STATUS(currentState, status: boolean){
      currentState.global.drawerStatus = status;
    }
    
  },
  actions: {
    updateRoom(context, roomData: Partial<IRoom>){
      context.commit('UPDATE_ROOM', roomData);
    },
    setDrawerStatus(context, status:boolean){
      context.commit('SET_DRAWER_STATUS', status);
    },
    updateClimateStatus(context, climate: IClimateStatus, roomId?: string){
      context.commit('UPDATE_TEMPERATURE', Object.assign({roomId}, climate));
    },
    setRoomId(context, roomId: string){
      // deregisterListeners(context.getters.getActiveRoom)
      context.commit('SET_ROOM_ID', roomId);
      // registerListeners(context.getters.getActiveRoom)
    },
    setUser(context, user: any){
      context.commit('SET_USER', user);
    },
    setRooms(context, rooms: Array<IRoom>){
      context.commit('SET_ROOMS', rooms);
      if(rooms.length > 0){
        context.commit('SET_ROOM_ID', rooms[0]['id']);
      }
    },
    setRoom(context, room: IRoom){
      context.commit('SET_ROOM', room);
    },
    addLoader(context){
      context.commit('SET_LOADER', 1);
    },
    removeLoader(context){
      context.commit('SET_LOADER', -1); 
    }
  },
  getters: {
    getDrawerStatus(store){
      return store.global.drawerStatus
    },
    getDarkMode(store){
      return store.global.dark
    },
    getFeed(store){
      return store.activeRoom
      ? prepareRoom(store, store.activeRoom)[store.activeRoom]
      : store.global
    },
    getUser(store){
      return store.user;
    },
    getLoader(store){
      return store.global.loader > 0;
    },
    getRooms(store): {[roomId: string]: {label: string, cards: Card[]} } {
      let dict = {} as any;
      Object.keys(store.rooms).forEach((roomId: string) => {
        dict = prepareRoom(store, roomId)
      });
      return dict;
    },
    getRoomsArray(store){
      return Object.keys(store.rooms).map(id => {
        return {id, ...store.rooms[id]};
      })
    },
    getRoom(state) {
      return (roomId) => this.getRooms(state)[roomId];
    },
    getActiveRoom(state){
      return state.rooms[state.activeRoom];
    },
    getState(store){
      return store;
    }
  }
});

function prepareRoom(store,roomId, dict = {}){
  const room = store.rooms[roomId];
  if(!room){
    return dict;
  }
  dict[roomId] = {};
  dict[roomId].id = roomId;
  dict[roomId].label = room.name;
  dict[roomId].label = room.name;
  dict[roomId].cards = room.feed && room.feed.map(feature => {
    switch(feature.type.toUpperCase()){
    case "IOT_CLIMATE":
    case "CLIMATE":
      return new Card(feature.corrolationId, CardTypes.CLIMATE, (feature.data && feature.data.label) || 'Room Climate', feature.data);
    case "IOT_CONTROL":
    case "CONTROL":
      return new Card(feature.corrolationId, CardTypes.CONTROL, (feature.data && feature.data.label) || 'Control', feature.data);
    default:
      return new Card(feature.corrolationId, feature.type, (feature.data && feature.data.label), feature.data);
    }
  });
  dict[roomId].cards = dict[roomId].cards || [];
  return dict;
}

function deregisterListeners(activeRoom){
  const ids = getListenersIds(activeRoom.id, activeRoom.features);
  if(Array.isArray(ids)){
    ids.forEach(id => feedService.deregisterFromEvent(id));
  }
  
}

function registerListeners(activeRoom){
  const ids = getListenersIds(activeRoom.id, activeRoom.features);
  ids.forEach(id => feedService.registerToEvent(id, data => this.$store.dispatch('updateFeature', {id, data}) ))
}

function getListenersIds(roomId, features){
  return features.map(feature => `${roomId}_${feature.corrolationId}`);
  
}