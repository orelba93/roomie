import Vue from 'vue';

Vue.filter('date', function(value){
    const options = { weekday: 'long', year: 'numeric', month: 'numeric', day: 'numeric', hour: '2-digit', minute:'2-digit' };
    return new Date(value).toLocaleDateString('en-GB', options );
});