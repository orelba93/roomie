import 'babel-polyfill';
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
import vuetify from './plugins/vuetify';
import ClimateControlService from './services/ClimateControlService';
import { AuthConfig, Auth0Plugin} from "./auth";
import './pipes';

const {clientId, domain} = AuthConfig;

new ClimateControlService().connect();
Vue.config.productionTip = false;
const deps = {
  router,
  store,
  vuetify,

};
Vue.use(
  Auth0Plugin, 
  {
    domain,
    clientId,
    onRedirectCallback: appState => {
      router.push("home");
    }
  }
)
new Vue({
  ...deps,
  ...{
    render: (h) => h(App)
  }
}).$mount('#app');


