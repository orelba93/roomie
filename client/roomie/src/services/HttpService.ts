import {Method} from 'axios';
import axios from 'axios';
import store from '../store';

export default class HttpService {
    private static BASE_URL = window.location.protocol+'//'+ window.location.host;

    public static get(uri: string, props?: any, headers?: any, toogleLoader?: boolean) {
        return this.request('get', uri, props, headers, null, toogleLoader);
    }

    public static post(uri: string, body: any, props?: any, headers?: any, toogleLoader?: boolean) {
        return this.request('post', uri, props, headers, body, toogleLoader);
    }

    public static put(uri: string, body: any, props?: any, headers?: any, toogleLoader?: boolean) {
        return this.request('put', uri, props, headers, body, toogleLoader);
    }

    public static delete(uri: string, props?: any, headers?: any, toogleLoader?: boolean) {
        return this.request('delete', uri, props, headers, null, toogleLoader);
    }

    private static request(method: Method,
                           uri: string,
                           props: any,
                           headers: any,
                           body: any,
                           toogleLoader: boolean = true) {
        const url = HttpService.BASE_URL + uri;
        store.dispatch("addLoader");
        return axios({
            method,
            url,
            params: props,
            data: body,
            headers
        }).finally( () => {
            store.dispatch("removeLoader");
        });
    }
}
