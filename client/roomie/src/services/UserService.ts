import HttpService from './HttpService';
import store, { IRoom } from '../store';
import FeedService from './FeedService';
import {getInstance} from '../auth';
import { SocketAbstract } from '../classes/socket.abstract';
import { FeedSocketEvents } from '../../../../common/feed/';
import { SocketMessage } from '../../../../common/core/classes/socket-message.class';

export default class UserService extends SocketAbstract{
    private static BASE_URI: string = '/user/';
    private static readonly singleton = new UserService()
    static cache: Promise<any>;
    static _socket: SocketIO.Client;

    protected get socket(): SocketIO.Client{
        return UserService._socket;
    }

    protected set socket(_socket: SocketIO.Client){
        UserService._socket = _socket;
    }
    onConnectCallback: () => void;
    


    public onConnect(): void {
        this.onConnectCallback();
    }
    public onDisconnect(): void {
    }

    public static addRoom(name: string, type: string){
        return HttpService.post(`${UserService.BASE_URI}room`, {name,type}).then(
            res => store.dispatch('setRooms', res.data)
        );
    }

    public static getUserData(){
        if(UserService.cache){
            return UserService.cache;
        }
        UserService.cache = Promise.all([
            HttpService.get(`${UserService.BASE_URI}`),
            FeedService.getFeedStatus()
        ])
        .then(  ([userDataRes, feedRes]) => {
            const rooms =  feedRes.data.feeds;
            store.dispatch('setUser', userDataRes.data.user);
            store.dispatch('setRooms', rooms);
            this.singleton.onConnectCallback = () => {
                this.singleton.events.push({name: FeedSocketEvents.UPDATE_FEED, callback: (event: SocketMessage) => store.dispatch("setRoom",event.data)});
                this.singleton.registerEvents()
                this.singleton.sendMessage('subscribe', new SocketMessage('REQUEST', {subjects: rooms.map(r => r.id)}));
            }
            this.singleton.connect();
            return {user: userDataRes.data.user, rooms: rooms};
        })
        return UserService.cache;
    }

    public static async setRoom(room: IRoom & {id: string}){
        room = await HttpService.put(`${UserService.BASE_URI}room/${room.id}`, room, {}, {} , false).then(res => res.data);
        store.dispatch('setRoom', room)
        return room;
    }

    public static async logout(){
        return Promise.all([
            HttpService.get(`${UserService.BASE_URI}logout`),
            getInstance().logout({returnTo: window.location.origin})
        ]);
    }
    public static async removeRoom(id: string){
        try{
            await HttpService.delete(`${UserService.BASE_URI}room/${id}`).then(res => res.data);
        }catch(e){
            throw e;
        }
        const rooms = store.getters.getRoomsArray;
        store.dispatch('setRooms', rooms.filter( room => room.id !== id));
    }
}