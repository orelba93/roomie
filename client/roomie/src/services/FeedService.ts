import HttpService from './HttpService';
import store, { IRoom } from '../store';
import { SocketAbstract } from '../classes/socket.abstract';
import { Card } from '../classes/card.class';

export default class FeedService  extends SocketAbstract{
    private static BASE_URI: string = '/feed/';
    static cache: Promise<any>;

    public static getFeedStatus(roomId?: string) {
        const uri = roomId ? `${FeedService.BASE_URI}status/${roomId}` : `${FeedService.BASE_URI}status`
        return HttpService.post(uri, {});
    }

    public static updateCard(card, roomId, cardId){
        const uri =`${FeedService.BASE_URI}feature/${roomId}/${cardId}`;
        return HttpService.put(uri, card);
    }

    public onConnect(): void {
    }
    public onDisconnect(): void {
    }
}