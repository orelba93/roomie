import { ClimateSocketEvents } from '../../../../common/climate/constants/climate-socket-events.enum';
import { SocketAbstract } from '../classes/socket.abstract';
import HttpService from './HttpService';
import store from '../store';

export default class ClimateControlService extends SocketAbstract{
    private static BASE_URI: string = '/control/';
    static _socket: SocketIO.Client;

    protected get socket(): SocketIO.Client{
        return ClimateControlService._socket;
    }

    protected set socket(_socket: SocketIO.Client){
        ClimateControlService._socket = _socket;
    }


    public static applyAction(roomId: string, action: string, payload?: {temp: number, swing: boolean}){
        return HttpService.post(`${ClimateControlService.BASE_URI}${roomId}/${action}`, payload);
    }

    constructor(){
        super()
    }

    connect(){
        super.connect();
        this.registerToEvent(ClimateSocketEvents.TEMPERATURE_EVENT, this.updateTemperature)
        return this;
    }

    public updateTemperature(eventData: any) {
        store.dispatch('updateClimateStatus', eventData);
    }

    public onConnect() {
     
    }
    public onDisconnect() {
     
    }

    
}