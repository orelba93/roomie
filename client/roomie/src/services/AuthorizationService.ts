import HttpService from "./HttpService";
import UserService from './UserService';
import store from '../store';

export default class AuthorizationService{
    // login(email: string, password: string){
    //     return HttpService.post('/login', {email, password});
    // }

    static async authorize(user){
        return await HttpService.post('/authorization/registerExternal', {
            firstName: user.given_name,
            lastName: user.family_name,
            picture: user.picture,
            email: user.email

        })
        .then(res => {
            const user = res.data;
            store.dispatch('setUser', user);
            UserService.getUserData()
            return res;
        });
    }
}
