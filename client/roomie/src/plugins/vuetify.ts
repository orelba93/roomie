import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import colors from 'vuetify/lib/util/colors'

const color1 = "#006d77";
const color2 = "#83c5be";
const color3 = "#edf6f9";
const color4 = "#ffddd2";
const color5 = "#e29578";

Vue.use(Vuetify);

export default new Vuetify({
  theme:{
    dark: false,//true,
    themes:{ 
      light: {
        primary:color1,
        secondary:color4,
        accent:color5,
        // error:color4,
        info:color3,
        // success:color5,
        // warning:color5
      },
    }
  },
  
  icons: {
    iconfont: 'mdi',
  },
});
