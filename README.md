# Docker

Install using docker-compose

	./roomie.sh start

# Client

Web UI Client application - VUE.js

	cd client/roomie
	npm run build

# Server

Web server for REST API and Socket handling - SugoiJS

Requires Redis and MongoDB

	cd server
	npm run start

# Pi-client

RespberryPI client applications

cd pi-client/src
